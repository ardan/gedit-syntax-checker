# Gedit Syntax Checker

Checks syntax upon file saving.

## Installation

Clone repo. Navigate into php-syntax-checker. Run ./install.sh

Open gedit then enable Gedit Syntax Checker plugin.

## Usage

Save a file. Errors will be displayed in the status bar and cursor will be moved to the offending line. Works best with line highlights turned on.

## Supported Files

Currently only support for PHP files is enabled.

## TODO

Enable syntax checking for additional file types.
