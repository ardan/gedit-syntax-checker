# For files ending in *.php, run the php -l command and display output in the statusbar.
# Also, move cursor to the line where the error occurred (works well with the current
# hightlight.

# TODO Rename plugin
# TODO Check other syntaxes other than PHP. For example, we can do composer.json, json.
# Create a json or xml file with the command and file extension
#   "command": "php -l",
#   "extension": "*.php"


# composer.json, *.json, *.css

from gi.repository import GObject, Gtk, Gedit
import re
import subprocess
import fnmatch
import pipes

class GeditPhpSyntaxCheckerGlobal:
    window = 0

class GeditPhpSyntaxCheckerWindow(GObject.Object, Gedit.WindowActivatable):

    __gtype_name__ = "GeditPhpSyntaxCheckerWindow"
    window = GObject.property(type=Gedit.Window)

    def __init__(self):
      GObject.Object.__init__(self)

    def do_activate(self):
      self.statusbar = self.window.get_statusbar()
      self.doc = self.window.get_active_document()
      GeditPhpSyntaxCheckerGlobal.window = self

    def do_deactivate(self):
      pass

class GeditPhpSyntaxChecker(GObject.Object, Gedit.ViewActivatable):
    __gtype_name__ = "GeditPhpSyntaxChecker"
    view = GObject.property(type=Gedit.View)
    line_num_re = re.compile(r"([0-9]+)$")

    def __init__(self):
        GObject.Object.__init__(self)

    def do_activate(self):
        self.doc = self.view.get_buffer()
        self.handler_id = self.doc.connect("saved", self.on_document_saving)
        self.context_id = GeditPhpSyntaxCheckerGlobal.window.statusbar.get_context_id('PHP_Syntax')

    def do_deactivate(self):
        self.doc.disconnect(self.handler_id)

    def do_update_state(self):
        pass

    def on_document_saving(self, *args):
        """On document saving handler"""
        path = self.doc.get_location().get_path()

        if fnmatch.fnmatch(path, "*.php") is False:
          return

        cmdline = "php -l" + " '" + pipes.quote(path) + "'"
        p = subprocess.Popen(cmdline, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell = True)
        output, errors = p.communicate()

        if p.returncode:
           self.hiliteLine(errors)
        else:
           self.addMsgToStatusbar("No syntax errors detected.")

    def addMsgToStatusbar(self, msg):
        # Remove messages at some point?
        self.message_id = GeditPhpSyntaxCheckerGlobal.window.statusbar.push(self.context_id, msg.strip())

    def hiliteLine(self, msg):
        match = self.line_num_re.search(msg)
        line = int(match.group(1)) - 1 # Line numbers start at 0
        itr = self.doc.get_iter_at_line(line)
        self.doc.place_cursor(itr)
        self.view.scroll_to_iter(itr, 0, True, 0, 0.5)
        self.addMsgToStatusbar(msg)
