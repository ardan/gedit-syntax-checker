#!/bin/sh
#
# Installs the plugin to the users folder

PLUGIN_FOLDER=~/.local/share/gedit/plugins/

# Install plugin
mkdir -pv $PLUGIN_FOLDER
cp -v gedit-syntax-checker.plugin $PLUGIN_FOLDER
cp -Rv gedit-syntax-checker $PLUGIN_FOLDER

echo "gedit-syntax-checker installed!"
